import React, { Fragment, useState } from "react";

type TodoKind = {
  id: string;
  isCompleted: boolean;
  todo: string;
};

const Todo = (props) => {
  const [input, setInput] = useState("");
  const [error, setError] = useState<boolean>(false);
  const [todoItems, setTodoItems] = useState<TodoKind[]>([]);

  const handleInputClick = (e) => {
    e.preventDefault();
    const value = e.target.value;
    if (!value) return;
    setInput(value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!input) {
      setError(true);
      return;
    }
    const _input: TodoKind = {
      todo: input,
      isCompleted: false,
      id: Math.random().toString(16).split(".")[1],
    };
    setTodoItems([_input, ...todoItems]);
    setInput("");
    setError(false);
  };

  const toggleCheck = (todoId) => {
    let todoIndex = todoItems.findIndex((_todo) => _todo?.id === todoId);
    let oldTodos = [...todoItems];
    let todo = oldTodos[todoIndex];
    todo.isCompleted = todo.isCompleted = !todo.isCompleted;
    setTodoItems(oldTodos);
  };

  const handleDelete = (todoId) => {
    const todos = todoItems.filter((_todo) => _todo.id !== todoId);
    setTodoItems(todos);
  };

  return (
    <Fragment>
      <div>
        <div
          className="w-full lg:w-full px-20 py-6 "
          style={{ backgroundColor: "#1e3046", color: "white" }}
        >
          <h2 className="text-center font-bold">ToDo App</h2>
        </div>
        <div className="container max-w-6xl max-w-6md py-6 p-6 mx-auto space-x-20 ">
          <div className=" w-full  bg-teal-lightest font-sans">
            {error && (
              <div className="bg-yellow-800 text-white py-3  my-2 mx-2 px-3 border-b-4 border-yellow-600 rounded-xl">
                please enter an item
              </div>
            )}
            <form className="flex-wrap flex flex-row items-center justify-center py-8">
              <div className="my-3 px-3 w-full overflow-hidden lg:w-1/2">
                <input
                  className="shadow appearance-none border rounded w-full py-2 px-3 mr-4 text-grey-darker"
                  placeholder="Add Todo"
                  onChange={handleInputClick}
                  value={input}
                ></input>
              </div>

              <div className="my-1 px-8 w-full sm:w-full lg:w-1/2">
                <button
                  className="w-full sm:w-full lg:w-1/2 bg-blue-500 text-white p-2 rounded text-2xl font-bold "
                  onClick={handleSubmit}
                >
                  Add item
                </button>
              </div>
            </form>
            {todoItems.map((todo, key) => (
              <div
                className="bg-white w-full flex items-center p-2 rounded-xl shadow border mb-2"
                key={key}
              >
                <input
                  onChange={() => toggleCheck(todo.id)}
                  type="checkbox"
                  checked={todo.isCompleted}
                />
                <div className="flex items-center space-x-4"></div>
                <div className="flex-grow p-3">
                  <div
                    className={
                      "font-semibold text-gray-700 text-center " +
                      (todo.isCompleted && "line-through")
                    }
                  >
                    {todo.todo}
                  </div>
                </div>
                <div className="p-2">
                  <button
                    className="inline-block p-3 text-center text-white transition bg-red-500 rounded-full shadow ripple hover:shadow-lg hover:bg-red-600 focus:outline-none"
                    onClick={(e) => {
                      e.preventDefault();
                      handleDelete(todo.id);
                    }}
                  ></button>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default Todo;
